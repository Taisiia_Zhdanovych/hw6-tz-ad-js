// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Асинхронність у JavaScript означає, що код може виконуватись паралельно, без блокування головного потоку виконання.
// Це означає, що JavaScript може виконувати багато задач одночасно, що забезпечує кращу відповідність користувацького 
// інтерфейсу та покращує загальну продуктивність програми.Це особливо корисно, коли потрібно виконати деякі довгі
// операції, такі як завантаження великих файлів або взаємодія з сервером, оскільки це не зупинить роботу програми.

const btn = document.querySelector(".btn");
btn.addEventListener("click", getInfo);

class UserLocation {
  constructor(continent, country, region, city, district) {
    
    this.continent = continent;
    this.country = country;
    this.region = region;
    this.city = city;
    this.district = district;
   
  }

    async getIP() {
     
    const responce = await fetch("https://api.ipify.org/?format=json");
    let res = await responce.json();
   
    return res.ip;
  }

   pageOn() {
         
          document.getElementById('info').insertAdjacentHTML(
            "afterbegin",
            `<ul>
       <li>Континент: ${this.continent}</li>
        <li>Країна: ${this.country}</li>
        <li>Регіон: ${this.region}</li>
        <li>Місто: ${this.city}</li>
        <li>Район: ${this.district}</li>
        </ul>`
          );
  }
  
    
    
  
}


  
 async function getInfo() {
    const user = new UserLocation();
    let result = await user.getIP();
   
  let requestIP = `http://ip-api.com/json/${result}?fields=1572885`;  
  let res = await fetch(requestIP);
  let locat = await res.json();
   

     Object.assign(user, locat);
   
  user.pageOn()


}
